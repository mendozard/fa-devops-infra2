#require 'vagrant-openstack-provider'

Vagrant.configure('2') do |config |
  # Openstack VMs definition.
  config.vm.provider :openstack do |os, override |
    os.identity_api_version = '3'
    os.openstack_auth_url = ENV['OS_AUTH_URL']
    os.domain_name = ENV['OS_DOMAIN_NAME']
    os.username = ENV['OS_USERNAME']
    os.password = ENV['OS_PASSWORD']
    os.project_name = ENV['OS_PROJECT_NAME']
    os.keypair_name = ENV['OS_KEY_PAIR_NAME']
    override.vm.synced_folder '.', '/vagrant', disabled: true
  end

  config.vm.define 'server-1' do |s|
    s.vm.provider :openstack do |os,override|
      override.ssh.username = 'ubuntu'
      override.ssh.private_key_path = ENV['OS_PRIVATE_KEY_PATH']
      os.server_name = "VM-#{ENV['OS_INITIALS']}-CI-01"
      os.image = 'ubuntu-18.04-amd64-server_28082018'
      os.flavor = 'student.2.4R'
    end
    s.vm.provision :docker do | docker |
      docker.pull_images "registry.jala.info/devops/jenkinsci/blueocean:1.18.1"
      docker.pull_images "registry.jala.info/devops/gocd/gocd-server:v19.7.0"
      docker.pull_images "registry.jala.info/devops/gocd/gocd-agent-alpine-3.10:v19.7.0"
    end
    s.vm.provision :file, source: "../docker/docker-compose.ci.yml", destination: "/home/ubuntu/docker-compose.yml"
    s.vm.provision :docker_compose, yml: "/home/ubuntu/docker-compose.yml", run: "always", compose_version: '1.24.1'
  end

  config.vm.define 'server-2' do |s|
    s.vm.provider :openstack do |os,override|
      override.ssh.username = 'ubuntu'
      override.ssh.private_key_path = ENV['OS_PRIVATE_KEY_PATH']
      os.server_name = "VM-#{ENV['OS_INITIALS']}-CI-02"
      os.image = 'ubuntu-18.04-amd64-server_28082018'
      os.flavor = 'student.2.4R'
    end
    s.vm.provision :docker do | docker |
      docker.pull_images "registry.jala.info/devops/sonarqube:7.9.1-community"
      docker.pull_images "registry.jala.info/devops/jfrog/artifactory-oss:latest"
    end
    s.vm.provision :file, source: "../docker/docker-compose.services.yml", destination: "/home/ubuntu/docker-compose.yml"
    s.vm.provision :docker_compose, yml: "/home/ubuntu/docker-compose.yml", run: "always", compose_version: '1.24.1'
  end

  config.vm.define 'server-3' do |s|
    s.vm.provider :openstack do |os,override|
      override.ssh.username = 'ubuntu'
      override.ssh.private_key_path = ENV['OS_PRIVATE_KEY_PATH']
      os.server_name = "VM-#{ENV['OS_INITIALS']}-CI-03"
      os.image = 'ubuntu-18.04-amd64-server_28082018'
      os.flavor = 'student.2.4R'
    end
    s.vm.provision :docker do | docker |
      docker.pull_images "registry.jala.info/devops/selenium/hub:3.141.59-titanium"
      docker.pull_images "registry.jala.info/devops/selenium/node-chrome:3.141.59-titanium"
      docker.pull_images "registry.jala.info/devops/selenium/node-firefox:3.141.59-titanium"
    end
    s.vm.provision "shell", path: "../scripts/install_tomcat9.sh"
    s.vm.provision :file, source: "../docker/docker-compose.testing.yml", destination: "/home/ubuntu/docker-compose.yml"
    s.vm.provision :docker_compose, yml: "/home/ubuntu/docker-compose.yml", run: "always", compose_version: '1.24.1'
  end
end
